\documentclass{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage[hidelinks]{hyperref}
\usepackage{listings}
\usepackage{pgf,tikz}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[default]{sourcesanspro}

\title{\huge Réseaux bayésiens - Projet\\
\Large Implémentation d'un réseau bayésien}
\author{\LARGE Elie \textsc{Kadoche}}
\date{15/05/2019}

\setlength{\parindent}{0pt}
\newcommand{\itemb}{\item[\( \bullet \)]}

\begin{document}

\maketitle

\tableofcontents

\clearpage

\section{Introduction}
Dans le cadre du master 1 décision à l'université Paris Dauphine, et dans le cadre de la matière ingénierie des connaissances avec Mme. Gabriella \textsc{Pigozzi}, j'ai dû effectuer un projet concernant les réseaux bayésiens. L'objectif de ce projet est d'une part, créer un réseau cohérent et trouver la valeur des probabilités conditionnelles de chaque n\oe ud, et d'autre part, implémenter une application permettant d'utiliser ce réseau de manière simple et efficace. Le c\oe ur du projet concerne l'algorithme d'élimination des variables, utilisé pour calculer le résultat d'une requête. Le présent rapport constitue la description de ce projet en donnant l'explication du réseau bayésien créé, l'architecture de l'application choisie, les détails techniques, les difficultés rencontrées et les évolutions futures de l'application.

\section{Un réseau bayésien autour de la santé}
\subsection{Une thématique cohérente}
Après de multiples recherches sur les domaines d'utilisation des réseaux bayésiens, je me suis vite rendu compte que la santé est un des candidats favoris. En effet les réseaux bayésiens sont très efficaces pour établir des diagnostics. Pour un individu donné, on a un ensemble d'informations qui correspondent aux symptômes observés, grâce auxquels, en utilisant le réseau, il est possible de trouver les diagnostics les plus probables. J'ai pu voir des réseaux immenses contenant des milliers de variables correspondant chacune à des symptômes techniques très spécifiques. Les données existantes sur les maladies et leur probabilité d'apparition en fonction de certains symptômes sont nombreuses. De plus, étant familier avec le domaine de la santé, j'ai finalement décidé d'effectuer mon graphe sur ce sujet.

\subsection{Les variables choisies}
Le c\oe ur du projet concernant l'algorithme d'élimination des variables, le graphe ne sert que d'outil pour illustrer son fonctionnement. J'ai donc pensé le graphe assez simple et peu technique. Il comporte 8 variables booléennes (pouvant prendre les valeurs \texttt{vrai} ou \texttt{faux}), chacune est représentée par une lettre différente (dans la suite du rapport nous utiliserons les lettres uniquement pour désigner les n\oe uds).
\begin{description}
    \item[Variable M] Revenu mensuel inférieur à 2099 euros par mois.
    \item[Variable O] L'individu est obèse.
    \item[Variable H] L'individu est un homme.
    \item[Variable A] L'âge de l'individu est supérieur à 65 ans.
    \item[Variable T] L'individu a de l'hypertension artérielle.
    \item[Variable F] L'individu est fumeur.
    \item[Variable S] L'individu est suicidaire.
    \item[Variable D] L'individu décède.
\end{description}
Donc quand la variable H est vraie, l'individu est un homme et quand elle fausse, l'individu est une femme. Quand la variable F est vraie, l'individu fume et quand elle est fausse, l'individu ne fume pas. Il en va de même pour toutes les variables.

\subsection{Les dépendances entre les variables}
Les variables O, T et D correspondent à des maladies et les variables M, H, A, F et S à des symptômes. Notons que la variable O, en plus d'être une maladie, est un symptôme pour T. De même que T est un symptôme pour D. Donc chaque maladie va dépendre de certains symptômes. Il sera ensuite pertinent lors de l'utilisation du réseau de connaître la probabilité d'avoir une maladie connaissant certains symptômes. Et inversement, la probabilité d'avoir un symptôme connaissant certaines maladies. Les dépendances sont donc décrites dans le graphe suivant.

\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale=1.5]
    \tikzset{node/.style={draw, circle, minimum width=0.5cm}}
    \tikzset{arrow/.style={->, >=stealth}}

    \node[node] (M) at (0,1) {M};
    \node[node] (H) at (0,0) {H};
    \node[node] (O) at (1.5,0.5) {O};
    \node[node] (T) at (4,1) {T};
    \node[node] (D) at (6,1) {D};
    \node[node] (F) at (3,0) {F};
    \node[node] (A) at (5,0) {A};
    \node[node] (S) at (7,0) {S};

    \draw (M) edge[arrow] node[right] {} (O);
    \draw (H) edge[arrow] node[right] {} (O);
    \draw (O) edge[arrow] node[right] {} (T);
    \draw (F) edge[arrow] node[right] {} (T);
    \draw (A) edge[arrow] node[right] {} (T);
    \draw (T) edge[arrow] node[right] {} (D);
    \draw (S) edge[arrow] node[right] {} (D);
\end{tikzpicture}
\end{figure}

\subsection{Les tables de probabilité}
Avoir des données réelles et existantes a été d'une telle importance que j'ai choisi les variables en fonction des données que j'ai trouvé sur internet. Voici donc les sources pour chaque variable. Quand une variable n'a pas de parents, sa table de probabilités correspond exactement aux données que j'ai trouvé. Dans le cas où une variable possède plusieurs parents, j'ai dû suivre une autre méthode (sauf pour la variable O). Pour expliquer cette dernière je prendrai un exemple.\\

La variable T dépend de O, F et A. Je connais les valeurs \(p(T|O)\), \(p(T|F)\) et \(p(T|A)\) mais je ne connais pas la valeur \(p(T|A,F,O)\) (ce que je cherche). Donc je trouve sa valeur en "m'inspirant" des 3 valeurs que j'ai. C'est le seul point qui est déplaisant d'un point de vue scientifique car les valeurs sont devinées en fonction de valeurs réelles. Cela est le cas pour les variables T et D.\\

Les tables de probabilité sont stockées dans un fichier JSON et doivent être chargées au lancement de l'application. Cela se fait via le menu "File" et "Load tables". Elles peuvent être chargées de nouveau à n'importe quel moment. Un modèle est donné dans le fichier \texttt{tables.json}. Il est donc possible de modifier la valeur des probabilités dans ce fichier et de le recharger dans l'application.

\paragraph{Les sources}
\begin{itemize}
    \itemb N\oe ud M : \href{https://www.inegalites.fr/Salaire-etes-vous-riche-ou-pauvre}{\color{blue}\texttt{lien}} pour le salaire des individus.
    \itemb N\oe ud H : \href{https://www.insee.fr/fr/statistiques/1892086?sommaire=1912926}{\color{blue}\texttt{lien}} pour la proportion des hommes et des femmes.
    \itemb N\oe ud A : \href{https://www.insee.fr/fr/statistiques/2381474}{\color{blue}\texttt{lien}} pour l'âge de la population.
    \itemb N\oe ud F : \href{https://www.santemagazine.fr/actualites/un-quart-des-fumeurs-souffrent-dhypertension-199531}{\color{blue}\texttt{lien}} pour la proportion de fumeurs.
    \itemb N\oe ud O : \href{https://www.lemonde.fr/planete/article/2016/10/25/un-francais-sur-deux-est-en-surpoids_5019615_3244.html}{\color{blue}\texttt{lien}} pour l'obésité en fonction du sexe et du salaire.
    \itemb N\oe ud T : \href{https://www.obesite.com/sante-et-obesite/hypertension/}{\color{blue}\texttt{lien}} pour la relation entre T et O, \href{https://www.inserm.fr/information-en-sante/dossiers-information/hypertension-arterielle-hta}{\color{blue}\texttt{lien}} pour la relation entre T et A.
    \itemb N\oe ud S : \href{http://www.santepubliquefrance.fr/Actualites/Suicide-et-tentative-de-suicides-donnees-nationales-et-regionales}{\color{blue}\texttt{lien}} pour les taux de suicide.
    \itemb N\oe ud D : \href{https://www.infosuicide.org/guide/le-phenomene-suicidaire/quelques-chiffres/}{\color{blue}\texttt{lien}} pour la relation entre D et S, \href{https://www.who.int/features/qa/82/fr/}{\color{blue}\texttt{lien}} pour la relation entre D et T, \href{http://www.ecology.com/birth-death-rates/}{\color{blue}\texttt{lien}} pour le nombre de morts.
\end{itemize}

\section{Une application simple et intuitive}
\subsection{Une inspiration de Bayes Server}
Avant de me lancer dans le développement de l'application, je me suis inspiré de logiciels déjà largement reconnus et mondialement utilisés. Je suis ainsi rapidement tombé sur celui de Bayes Server que j'ai tout de suite aimé (un exemple est donné \href{https://www.bayesserver.com/examples/networks/asia}{\color{blue}\texttt{ici}}). Pour chaque n\oe ud on peut voir 2 valeurs qui correspondent aux probabilités conditionnelles de la variable. Pour une variable \( x_{i} \), les 2 valeurs correspondent donc respectivement à \( p(x_{i} | \textit{known\_variables}) \) et \( p(\lnot x_{i} | \textit{known\_variables})\). Ainsi, je trouve que l'utilisation de l'application est ludique et intuitive : en fonction des données que l'on a, on peut voir les données du réseau évoluer.

\subsection{L'utilisation de l'application}
Une capture d'écran (\ref{interface}) de l'application est donnée en annexe. Le principe est donc de représenter chaque variable dans un n\oe ud du graphe. Pour chaque variable nous avons 3 possibilités :
\begin{itemize}
	\itemb on sait que la variable est vraie : alors on coche la case correspondant à la valeur \texttt{True}.
	\itemb On sait que la variable est fausse : alors on coche la case correspondant à la valeur \texttt{False}.
	\itemb On ne connaît pas la valeur de la variable : alors on ne coche aucune des 2 cases du n\oe ud.\\
\end{itemize}

\`{A} chaque fois qu'une donnée du réseau est modifiée (donc à chaque fois qu'une case est cochée ou décochée) l'application va calculer pour chaque variable X non connue, la probabilité que la variable X soit vraie (et fausse) sachant les variables vraies et fausses du réseau. Par exemple pour un réseau de 8 variables, si après avoir coché la case d'un n\oe ud, on sait que 2 variables sont vraies et que 3 sont fausses alors l'application va lancer \( 8 - 2 - 3 = 3\) algorithmes d'élimination des variables pour déterminer les probabilités de chaque variable X inconnue.

\section{L'architecture de l'application}
\subsection{Python comme langage de programmation}
Le choix du langage de programmation a été rapide. L'objectif de ce projet est de comprendre le fonctionnement profond de l'algorithme d'élimination des variables en l'implémentant sans utiliser de librairies spécifiques. Python étant simple à utiliser et simple à lire et n'ayant pas de contraintes de performance, j'ai donc décidé de l'utiliser pour coder. Le code est commenté et écrit en anglais.

\subsection{Une architecture modèle - vue - contrôleur}
J'ai décidé de suivre une architecture modèle - vue - contrôleur (MVC) pour l'application. Je n'ai peut-être par réalisé rigoureusement ce modèle mais je m'en suis inspiré afin de rendre le code plus propre, plus robuste aux modifications et facile à tester. Il s'agit de séparer la partie graphique et les données de l'application. Ainsi, il est possible de travailler sur chaque élément de manière indépendante. J'ai donc séparé mon code en 3 éléments principaux, expliqués ci-après.

\paragraph{Le modèle}
Le modèle correspond aux données de l'application nécessaires à son fonctionnement profond. Le modèle représente grossièrement tout ce qui ne concerne par l'interface graphique. Nous y retrouverons la représentation des variables ({\color{blue}\ref{var}}) ainsi que l'algorithme d'élimination des variables ({\color{blue}\ref{algo}}) que nous détaillerons plus loin.

\paragraph{La vue}
La vue correspond à l'interface graphique. Elle est donc indépendante des données (du modèle). Tout le code gérant l'interface se trouve dans le fichier \texttt{application.py}. Je serai bref sur l'explication de l'interface car ce n'est pas le c\oe ur du sujet. Il y a une classe \texttt{Node\_GUI} qui correspond à la représentation graphique d'une variable sur l'interface. Les fonctions de \texttt{callback} sont particulièrement importantes. Ce sont celles qui sont lancées dès que l'utilisateur coche une case (\texttt{True} ou \texttt{False}) d'un n\oe ud. Elles lancent alors l'algorithme d'élimination des variables.

\paragraph{Le contrôleur}
Le contrôleur est le module qui permet de faire la transition entre les 2 éléments indépendants, précédemment décrits : le modèle et la vue. Quand l'utilisateur coche une case sur l'interface, il modifie alors les données, il faut donc indiquer au modèle les modifications. Et inversement, quand l'algorithme d'élimination des variables est lancé, les valeurs des probabilités des variables changent. Il faut donc ensuite l'indiquer à la vue pour qu'elle modifie les valeurs sur l'interface graphique. C'est le contrôleur qui permet de faire cette transition entre le modèle et la vue. Le contrôleur est une petite classe écrite dans le fichier \texttt{application.py}.

\subsection{Les tests unitaires}
Le fait d'avoir séparer le modèle de la vue m'a permis de réaliser des tests unitaires. Pour une première version de l'application, j'ai mélangé interface et données et je me suis vite rendu compte que je ne pouvais que difficilement réaliser des tests sur mon code. J'ai donc rapidement basculé sur une architecture MVC qui m'a simplifié bien des choses. Les tests unitaires sont des petites fonctions qui permettent de vérifier que les fonctions utilisées dans l'application fonctionnent bien. Je n'ai effectué des tests que sur les opérations utilisées dans l'algorithme d'élimination des variables.\\

Ces tests ont été la raison principale de mon avancée. C'est grâce à eux que j'ai pu savoir quand est-ce que mes méthodes fonctionnaient, et quand ce n'était pas le cas, pourquoi. C'est eux qui m'ont permis de déboguer les opérations de l'algorithme d'élimination des variable. Sans eux il m'aurait été très difficile de comprendre mes erreurs, de les corriger et d'avancer. Les tests unitaires sont une preuve de fonctionnement du code et m'ont permis d'être beaucoup plus efficace. Ils sont tous écrits dans le fichier \texttt{tests.py}.

\subsection{\label{var}La représentation des variables}
Pour représenter les variables du modèle j'ai décidé de créer une simple classe \texttt{Node}. Toutes les variables sont créées dans le fichier \texttt{nodes.py}. Une variable est caractérisée par plusieurs éléments.
\begin{itemize}
	\itemb \texttt{identifier}, une lettre unique représentant la variable.
	\itemb \texttt{name}, la description plus longue de la variable.
	\itemb \texttt{var\_table}, une chaîne de caractères contenant les \texttt{identifier} des parents de la variable considérée ainsi que l'\texttt{identifier} de la variable considérée. Leur ordre est particulièrement important car c'est lui qui indique à quelle variable correspondent les valeurs dans la table de probabilité.
	\itemb \texttt{table}, c'est la table de probabilité de la variable. C'est un dictionnaire qui associe des tuples de booléens (séquence de valeurs booléennes) à une valeur.\\
\end{itemize}

Par souci de simplicité, j'ai décidé de mettre dans les tables toutes les combinaisons possibles pour la variable. Ainsi, on peut obtenir directement la valeur d'un probabilité quand la variable est fausse, au lieu de faire \( 1 - p\)(probabilité quand la variable est vraie). Voici un exemple concret, tiré du modèle de l'application. Cette représentation des données est simple mais elle m'a été largement suffisante pour effectuer toutes les opérations de l'algorithme d'élimination des variables.

\begin{minipage}[c]{0.6\textwidth}
	La variable considérée est O : \texttt{identifier} = 'O'.\\
	Ses parents sont 'MH' donc \texttt{var\_table} = 'OMH'.\\
	La ligne \((True,False,True):0.07\) signifie : \( p(o|\lnot m, h) = 0.07\).\\
	La ligne \((False,True,False):0.507\) signifie : \( p(\lnot o|m, \lnot h) = 0.507\).
\end{minipage}
\begin{minipage}[c]{0.4\textwidth}
	\begin{align*}
	\{ &(True,True,True):0.374,\\
	&(True,True,False):0.493,\\
	&(True,False,True):0.91,\\
	&(True,False,False):0.07,\\
	&(False,True,True):0.626,\\
	&(False,True,False):0.507,\\
	&(False,False,True):0.09,\\
	&(False,False,False):0.93\}
	\end{align*}
\end{minipage}

\section{\label{algo} L'algorithme d'élimination des variables}
Cette partie fait l'objet d'une section à part entière du fait de son importance. L'algorithme d'élimination des variables représente le c\oe ur de ce projet. Tout son code est écrit dans le fichier \texttt{variable\_elimination.py} et les méthodes sont testées dans le fichier \texttt{tests.py}. Nous ne détaillerons pas le détail de toutes les opérations dans ce rapport, le code étant largement commenté.

\subsection{La classe \texttt{Variable\_elimination}}
Un unique objet \texttt{variable\_elimination} est créé pour l'ensemble de l'application. Il contient un seul élément : un dictionnaire qui associe les \texttt{identifier} de chaque variable du réseau à l'objet \texttt{node} qui lui correspond. La classe possède 2 méthodes :
\begin{itemize}
	\itemb \texttt{launch\_algorithm}, c'est la fonction qui va lancer l'algorithme d'élimination des variables pour chaque variable inconnue du réseau;
	\itemb \texttt{variable\_elimination\_algorithm}, c'est la fonction qui correspond finalement à l'algorithme d'élimination des variables. Elle prend en paramètre le n\oe ud correspondant à la variable dont on cherche la probabilité. C'est elle qui met à jour les valeurs des probabilités d'un n\oe ud.
\end{itemize}

\subsection{L'ordre des opérations}
L'algorithme d'élimination des variables est caractérisé par un ensemble d'opérations différentes, le produit de tables de probabilité et la marginalisation sont les principales. Quand on fait l'algorithme d'élimination des variables sur papier, on effectue normalement les produits et les marginalisations dans un certain ordre. Après quelques recherches, je me suis vite rendu compte que trouver cet ordre peut être un problème très difficile quand il y a beaucoup de variables. J'ai donc décidé de ne pas prendre en compte cet ordre car cela ne modifie pas le résultat de l'algorithme.\\

Cela signifie qu'en premier lieu j'effectue tous les produits de table. Et ce n'est qu'en second lieu que j'effectue toutes les marginalisations. Je m'évite ainsi le calcul pour trouver l'ordre des opérations. Les conséquences sont les suivantes :
\begin{itemize}
	\itemb le code est plus simple à lire et à comprendre;
	\itemb le temps de calcul est à priori plus rapide surtout pour des réseaux où le nombre de variable est très grand;
	\itemb mais il y a beaucoup plus d'espace mémoire utilisé car l'algorithme doit gérer des tables très grandes. Dans le pire des cas, si on a \(N\) variables on aura une table de \(2^{N}\) lignes (256 lignes pour 8 variables par exemple).
\end{itemize}

\subsection{Les opérations utilisées}
\paragraph{\texttt{remove\_known\_values\_to\_get\_f\_table}}
Cette fonction permet de récupérer la table d'une variable en considérant les variables connues du graphe. Par exemple si nous devons récupérer la table de la variable T. La table initiale contient les valeurs pour toutes les combinaisons possibles entre les variables T, F, O et A. Mais si on sait que la variable F est vraie et la variable A est fausse alors nous allons devoir apporter une modification à cette table, c'est-à-dire retirer toutes les lignes de la table où la variable F est fausse et la variable A est vraie. C'est donc ce que fait cette fonction.

\paragraph{\texttt{factor\_product}}
Cette fonction permet de prendre 2 tables et d'en calculer le produit. Elle prend bien en compte les variables que les 2 tables ont en commun.

\paragraph{\texttt{marginalize}}
Cette fonction permet, selon une table et une variable données, de marginaliser la table sur cette variable. Cela signifie que si on a une table selon les variables O, M et H et qu'on souhaite la marginaliser sur O, on va se retrouver avec une table contenant seulement les variables M et H. Marginaliser revient à faire la somme des probabilités quand O est vraie et fausse, mais en considérant toujours les autres.

\paragraph{\texttt{remove\_known\_values}}
Cette fonction permet de supprimer les variables connues de la table finale obtenue. \`{A} la fin de l'algorithme d'élimination des variables, on se retrouve avec une table contenant 2 valeurs, vraie et faux pour la variable dont on cherche les valeurs. Mais nous avons également toutes les variables connues du réseau avec leur valeur. Par exemple si on connaît la variable H (vraie) et M (fausse), et que l'on cherche la variable D alors on obtient à la fin de l'algorithme la table suivante avec \texttt{var\_table} = 'DMH'.\\

\begin{minipage}[c]{0.5\textwidth}
	On peut voir que l'on a D qui prend les valeurs vrai et faux mais que H et M sont fixes.
\end{minipage}
\begin{minipage}[c]{0.5\textwidth}
	\begin{align*}
	\{ &(True,True,False):0.07,\\
	&(False,True,False):0.93\}
	\end{align*}
	\hspace{1pt}
\end{minipage}

\begin{minipage}[c]{0.5\textwidth}
	On les retire donc juste pour se retrouver avec la table suivante avec \texttt{var\_table} = 'D'.
\end{minipage}
\begin{minipage}[c]{0.5\textwidth}
	\begin{align*}
	\{ &(True,):0.07,\\
	&(False,):0.93\}
	\end{align*}
	\hspace{1pt}
\end{minipage}
Cette étape est importante car elle permet de faciliter la dernière opération de l'algorithme, l'opération "alpha". On fait la somme des 2 probabilités. Puis on divise chaque probabilité obtenue par la somme afin d'obtenir les résultats finaux.

\section{Les difficultés rencontrées}
\subsection{L'interface graphique, petites difficultés}
C'est la première fois que j'ai réalisé une interface graphique sur Python, avec TKinter. J'étais d'ores-et-déjà familier avec le principe de fonctionnement des librairies graphiques (sur Java notamment). Mais j'ai passé du temps à comprendre les outils de TKinter nécessaires pour réaliser des flèches par exemple. C'est la seule partie du projet sur laquelle je me suis fait aider. Après que l'on m'est expliqué les différents objets existants sur TKinter et en lisant la documentation j'ai ensuite rapidement pu avancer pour réaliser l'interface que je souhaitais. J'ai mis du temps à placer les n\oe uds où je voulais mais je suis finalement plutôt content du résultat.

\subsection{L'algorithme d'élimination des variables, plus grandes difficultés}
L'algorithme d'élimination des variables m'a pris beaucoup de temps. Durant plusieurs jours d'affilés je n'ai pas été capable d'écrire une ligne de code, ne sachant pas comment modéliser informatiquement les données et les opérations que j'arrivais à faire sur papier. J'ai finalement décidé de partir sur une structure de données simples (chaîne de caractères pour les variables dont dépendent la table de probabilité d'un n\oe ud, elle-même représentée par un dictionnaire).\\

J'ai passé beaucoup de temps à réfléchir comment trouver l'ordre des opérations. Et lorsque j'ai finalement pris la décision de ne pas m'en préoccuper, j'ai pu avancer plus rapidement. J'ai alors commencé à coder chaque opération une par une. Et comme je l'ai dit plus haut, ce sont les tests qui m'ont permis d'avancer très rapidement. J'ai codé chaque opération une à une, avant même d'avoir fait l'algorithme d'élimination des variables.

\paragraph{Test Driven Development}
J'ai suivi en partie ce qu'on appelle du \emph{TDD} (Test Driven Development), c'est-à-dire, développer en fonction des tests. Il s'agit de coder les tests avant toute chose, puis ensuite réaliser la fonction jusqu'à ce que les tests passent. En faisant ainsi, j'étais sûr d'avoir coder toutes les opérations correctement.\\

Et ce n'est qu'après avoir coder chaque opération que j'ai pu réaliser l'algorithme d'élimination des variables, qui lui, fut très rapide à faire. Dès lors que j'avais chaque opération fonctionnelle, la fin s'est faite très rapidement.

\section{Limites et évolutions de l'application}
\subsection{Le calcul des probabilités jointes}
Par rapport au projet ainsi que par rapport à l'application, une lacune majeure de l'application est de ne pas pouvoir calculer des probabilités jointes. Par exemple \( p(M,H|o, \lnot f) \) qui donnerait une table de probabilité jointe entre \(M\) et \(H\). En effet, l'application ne donne que les probabilités de \( p(X| \textit{known\_variables} ) \) avec \(X\) correspondant toujours à une seule variable et prenant ses valeurs dans l'ensemble de toutes les variables inconnues.\\

Faute de temps, je n'ai pas développé cette amélioration pour 2 raisons :
\begin{itemize}
	\itemb je voulais que l'application soit simple et intuitive. Je considère que le calcul des probabilités jointes n'est pas une fonctionnalité essentielle. Elle n'est pas très intuitive et pas visuelle.
	\itemb Second point, dans le sujet il est bien spécifié à la question 3), "[...] et de spécifier quelle variable doit être interrogée". Il est donc bien question d'une seule variable et non de plusieurs.\\
\end{itemize}

Mais bien évidemment, cette fonctionnalité manque tout de même et sera à fairer dans le cadre d'un développement futur de l'application. C'est l'amélioration la plus simple et rapide à faire.

\subsection{Une interface graphique adaptative}
Le dernier point concerne le point le plus important des limites et évolutions de l'application. Dans l'idéal, j'aurais aimé pouvoir réaliser une application ayant la capacité de prendre n'importe quel réseau bayésien et de le faire tourner sur son interface. C'est d'ailleurs en partie dans cette optique là que j'ai voulu bien séparer mon modèle de ma vue, car en faisant ainsi, je peux utiliser le même modèle, fonctionnel, sur n'importe quelle interface. Ces 2 éléments sont indépendants.\\

La limite vient donc de la vue, qui ne fonctionne présentement que pour un réseau bayésien, celui donné dans l'application. Mes connaissances en développement d'interface graphique étant trop limitées, je n'ai pas pu réaliser d'interface s'adaptant à n'importe quel réseau. Cependant mon modèle fonctionne pour n'importe quel réseau bayésien, et il me faudrait juste refaire la vue pour la rendre modulable en fonction du réseau. Le modèle est déjà prévu pour récupérer les données d'un fichier JSON, il faudrait juste en faire de même pour l'interface graphique.

\section{Conclusion}
Ce projet a été pour moi un véritable défi. J'ai été confronté à 2 difficultés qui m'ont permis de parfaire mes connaissances en informatique et d'affiner ma compréhension de l'algorithme d'élimination des variables. Réaliser ce projet seul a été un challenge, lorsque je bloquais sur une partie, il n'y avait personne pour m'aider. Ce projet m'a aidé à prendre du recul par rapport à mon travail et à trouver d'autres manières de faire. J'ai également pu confirmer l'importance des tests dans le développement d'un algorithme complexe. Sans eux je n'aurais pas pu avancer, et même s'il peut être laborieux et pas très amusant de les faire, leur apport en terme d'efficacité et de rentabilité est tel, que s'en priver serait à mon sens, une erreur de développement.\\

Finalement, ce projet m'a apporté beaucoup de satisfaction personnelle. Pendant longtemps je pensais ne pas pouvoir réussir à le réaliser, et finalement avec beaucoup d'investissement j'ai réussi à arriver à mes fins. C'est un projet dont je suis donc fier et j'ai pu améliorer mes compétences sur Python, langage que j'affectionne particulièrement et que je serai amené à utiliser beaucoup plus souvent. J'ai également pu découvrir la puissance que pouvaient avoir des réseaux bayésiens avec des milliers de variables, outils que je serai probablement amené à réutiliser.

\clearpage

\section*{Annexe}
\begin{figure}[!h]
	\begin{center}
		\includegraphics[scale=0.5]{interface.png}
		\caption{\label{interface} Interface de l'application}
	\end{center}
\end{figure}

\begin{figure}[!h]
	\begin{center}
		\includegraphics[scale=0.4]{security.png}
		\caption{Pour ouvrir la 1\iere{} fois l'application sur Mac, il faut aller dans les préférences système, onglet sécurité et cliquer sur le bouton "Ouvrir quand même"}
	\end{center}
\end{figure}

\end{document}

