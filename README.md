# bayesian_network

![demo](materials/demo.gif)

Graphical implementation of a bayesian network with the variable elimination algorithm coded from scratch, without any specific library.
Project made the 15/05/2019 for a master 1 class.

## Requirements

Python 3.6 or a more recent version.
The only used external packages are Tkinter for the GUI, pytest for the tests and pyinstaller to build the application.
The entire algorithm is build from scratch.

## Project structure

- `materials`: a French description of the subject and the proposed implementation.
- `src`: the entire source code.
- `tables`: json file of the probability tables.

# Usage

Run.

```
python main.py
```

Create executable

```
pyinstaller --onefile main.py
```

# To run tests

Tests were the key of this project.
Without them I would not be able to develop the correct functions.

```
pytest src/test.py
```
