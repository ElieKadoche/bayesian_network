import itertools
from copy import deepcopy


class Variable_elimination:
    """Variable elimination algorithm."""

    def __init__(self):
        self.nodes_dict = dict()

    def add_node(self, node):
        self.nodes_dict[node.identifier] = node

    def launch_algorithm(self):
        # First, we need to know known and unkown nodes
        for identifier, node in self.nodes_dict.items():

            # If the node is known as true
            if node.is_true:
                node.true_value = 100
                node.false_value = 0.0

            # If the node is known as false
            elif node.is_false:
                node.true_value = 0.0
                node.false_value = 100

            else:
                # We apply the variable_elimination algorithm on each unknown
                # node
                self.variable_elimination_algorithm(node)

    def variable_elimination_algorithm(self, target_node):
        # We find all marginalizations
        nodes_to_be_marginalized = ""
        for identifier, node in self.nodes_dict.items():
            # If the node is unkown and is not the node we want to calculate
            if (
                not node.is_true
                and not node.is_false
                and identifier != target_node.identifier
            ):
                nodes_to_be_marginalized = nodes_to_be_marginalized +\
                    identifier

        # We calculate tables
        f_tables_list = list()  # List of tuple of all f_tables
        for identifier, node in self.nodes_dict.items():
            var_f_table, f_table = remove_known_values_to_get_f_table(
                self.nodes_dict, node.var_table, node.table
            )
            f_tables_list.append((var_f_table, f_table))

        # We calculate the product of all the table
        while len(f_tables_list) > 1:
            tuple_a = f_tables_list.pop()
            tuple_b = f_tables_list.pop()

            # We calculate the new f_table
            new_var_f_table, new_f_table = factor_product(
                self.nodes_dict, tuple_a[0], tuple_a[1], tuple_b[0], tuple_b[1]
            )

            # And we add it the list
            f_tables_list.append((new_var_f_table, new_f_table))

        # We are left with one single (big) table
        final_element = f_tables_list.pop()
        final_var_table = final_element[0]
        final_table = final_element[1]

        # Now we marginalize
        for var in nodes_to_be_marginalized:
            final_var_table, final_table = marginalize(
                var, final_var_table, final_table
            )

        # And we remove values of known variables
        # This step is not necessary: at the end, we have only 2 values, True
        # and False for the wanted node. But we also have all others variable
        # with their values, for example we know variable H (True) and M
        # (False), and we search for value D: {(True,True,False): 0.07,
        # (False,True,False): 0.93} with var_table == 'DHM'. We can see that we
        # have D with values True and False, but H and M are fixed. So we just
        # remove them at the end to obtain: {(True,): 0.07, (False,): 0.93}
        # with var_table == 'D'
        final_var_table, final_table = remove_known_values(
            self.nodes_dict, final_var_table, final_table
        )

        # We are left with two probabilities
        value_when_true = final_table[(True,)]
        value_when_false = final_table[(False,)]
        sum_values = value_when_true + value_when_false
        value_when_true = (value_when_true / sum_values) * 100
        value_when_false = (value_when_false / sum_values) * 100
        target_node.true_value = round(value_when_true, 2)
        target_node.false_value = round(value_when_false, 2)


# All possible operations
# ------------------------------------------


def remove_known_values_to_get_f_table(nodes_dict, var_table, original_table):
    # To create the f_table, we need te remove all known variables
    # For example, p(D|T,f,s) will have a f_table depending only on D and T, f
    # and s being always true

    table = deepcopy(original_table)

    # Position of the variable
    position = 0

    for var in var_table:
        # The variable is known as true so we remove every case where the
        # variable is false
        if nodes_dict[var].is_true:
            for key in dict(table).keys():
                if not key[position]:
                    del table[key]

        # The variable is known as false so we remove every case where the
        # variable is true
        if nodes_dict[var].is_false:
            for key in dict(table).keys():
                if key[position]:
                    del table[key]

        position += 1

    return var_table, table


def factor_product(nodes_dict, var_table_a, f_table_a, var_table_b, f_table_b):
    var_table = var_table_a + var_table_b
    var_table = "".join(set(var_table))  # We remove duplicate
    # Alphabetical order (to make tests easier)
    var_table = "".join(sorted(var_table))
    table_init = dict()

    # Find all permutations possible for the new table
    ingredients = [True, False]
    permutations_list = tuple(
        (itertools.product(ingredients, repeat=len(var_table))))

    # We initialize the new table
    for new_key in permutations_list:
        table_init[new_key] = 0

    # We remove permutations not present in the two tables (in the case where a
    # variable is true for example)
    var_table, table = remove_known_values_to_get_f_table(
        nodes_dict, var_table, table_init
    )

    # We add permutations to the table
    for key in table:
        # Current variables values of the final table are kept in a dictionary
        position = 0
        variables_values = dict()
        for var in var_table:
            variables_values[var] = key[position]
            position += 1

        # Value in table_a
        # Tuple initialized of the size of var_table_a
        key_in_table_a = tuple([None] * len(var_table_a))
        position = 0
        for var in var_table_a:
            # We update the key with the value of the variable
            key_in_table_a = (
                key_in_table_a[:position]
                + (variables_values[var],)
                + key_in_table_a[position + 1:]
            )
            position += 1
        value_in_table_a = f_table_a[key_in_table_a]

        # Value in table_b
        # Tuple initialized of the size of var_table_b
        key_in_table_b = tuple([None] * len(var_table_b))
        position = 0
        for var in var_table_b:
            # We update the key with the value of the variable
            key_in_table_b = (
                key_in_table_b[:position]
                + (variables_values[var],)
                + key_in_table_b[position + 1:]
            )
            position += 1
        value_in_table_b = f_table_b[key_in_table_b]

        # Value in table
        value = value_in_table_a * value_in_table_b
        table[key] = value

    return var_table, table


def marginalize(identifier, original_var_table_entry, original_table_entry):
    """Sum on a variable."""
    original_var_table = deepcopy(original_var_table_entry)
    original_table = deepcopy(original_table_entry)

    position = original_var_table.find(identifier)
    var_table = original_var_table.replace(identifier, "")
    table = dict()

    for key, value in dict(original_table).items():
        # When the variable is true
        if key[position]:
            value_when_true = value

            # Key when the variable is false
            key_when_false = key[:position] + (False,) + key[position + 1:]
            value_when_false = original_table[key_when_false]

            # We calculate the marginalized value
            key_when_marginalized = key[:position] + key[position + 1:]
            marginalized_value = value_when_true + value_when_false
            table[key_when_marginalized] = marginalized_value

            del original_table[key]
            del original_table[key_when_false]

    return var_table, table


def remove_known_values(nodes_dict, var_table, table):
    for var in var_table:
        if nodes_dict[var].is_true or nodes_dict[var].is_false:
            # We find the position of the variable
            var_position = var_table.find(var)

            # We remove the variable value
            for key, value in dict(table).items():
                new_key = key[:var_position] + key[var_position + 1:]
                table[new_key] = table.pop(key)

            # We remove the variable from var_table
            var_table = var_table.replace(var, "")

    return var_table, table
