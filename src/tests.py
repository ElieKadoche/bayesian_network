from copy import deepcopy

from src.nodes import (a_node, d_node, f_node, get_table, h_node, m_node,
                       o_node, s_node, t_node)
from src.variable_elimination import (Variable_elimination, factor_product,
                                      marginalize,
                                      remove_known_values_to_get_f_table)

variable_elimination = Variable_elimination()

variable_elimination.add_node(m_node)
variable_elimination.add_node(h_node)
variable_elimination.add_node(o_node)
variable_elimination.add_node(a_node)
variable_elimination.add_node(f_node)
variable_elimination.add_node(t_node)
variable_elimination.add_node(d_node)
variable_elimination.add_node(s_node)

path = "tables/tables.json"

m_node.initialized_table(get_table("M", path))
h_node.initialized_table(get_table("H", path))
o_node.initialized_table(get_table("O", path))
a_node.initialized_table(get_table("A", path))
f_node.initialized_table(get_table("F", path))
t_node.initialized_table(get_table("T", path))
d_node.initialized_table(get_table("D", path))
s_node.initialized_table(get_table("S", path))


def test_remove_known_values_to_get_f_table_h():
    temp_nodes_dict = deepcopy(variable_elimination.nodes_dict)
    temp_nodes_dict["H"].is_true = True

    var_table = h_node.var_table
    original_table = h_node.table

    var_f_table, f_table = remove_known_values_to_get_f_table(
        temp_nodes_dict, var_table, original_table
    )
    f_table_expected = {(True,): 0.48355}

    assert f_table == f_table_expected
    assert var_f_table == "H"


def test_remove_known_values_to_get_f_table_o():
    temp_nodes_dict = deepcopy(variable_elimination.nodes_dict)
    temp_nodes_dict["M"].is_true = True
    temp_nodes_dict["H"].is_false = True

    var_table = o_node.var_table
    original_table = o_node.table

    var_f_table, f_table = remove_known_values_to_get_f_table(
        temp_nodes_dict, var_table, original_table
    )
    f_table_expected = {(True, True, False): 0.493,
                        (False, True, False): 0.507}

    assert f_table == f_table_expected
    assert var_f_table == "OMH"


def test_remove_known_values_to_get_f_table_t():
    temp_nodes_dict = deepcopy(variable_elimination.nodes_dict)
    temp_nodes_dict["F"].is_true = True

    var_table = t_node.var_table
    original_table = t_node.table

    var_f_table, f_table = remove_known_values_to_get_f_table(
        temp_nodes_dict, var_table, original_table
    )
    f_table_expected = {
        (True, True, True, True): 0.73,
        (True, True, True, False): 0.44,
        (True, True, False, True): 0.49,
        (True, True, False, False): 0.25,
        (False, True, True, True): 0.27,
        (False, True, True, False): 0.56,
        (False, True, False, True): 0.51,
        (False, True, False, False): 0.75,
    }

    assert f_table == f_table_expected
    assert var_f_table == "TFOA"


def test_maginalize_o():
    original_var_table = o_node.var_table
    original_table = o_node.table

    marginalized_var_table, marginalized_table = marginalize(
        "O", original_var_table, original_table
    )
    marginalized_table_expected = {
        (True, True): 1.0,
        (True, False): 1.0,
        (False, True): 1.0,
        (False, False): 1.0,
    }

    assert marginalized_var_table == "MH"
    assert marginalized_table == marginalized_table_expected


def test_maginalize_h():
    original_var_table = h_node.var_table
    original_table = h_node.table

    marginalized_var_table, marginalized_table = marginalize(
        "H", original_var_table, original_table
    )
    marginalized_table_expected = {(): 1.0}

    assert marginalized_var_table == ""
    assert marginalized_table == marginalized_table_expected


def test_factor_product_ho():
    nodes_dict_temp = deepcopy(variable_elimination.nodes_dict)

    var_table_a = h_node.var_table
    table_a = h_node.table

    var_table_b = o_node.var_table
    table_b = o_node.table

    var_table, table = factor_product(
        nodes_dict_temp, var_table_a, table_a, var_table_b, table_b
    )

    # To make test easy we round values
    for key, value in table.items():
        table[key] = round(value, 3)

    assert var_table == "HMO"

    # Variables: H - M - O
    assert table == {
        (True, True, True): 0.181,
        (False, True, True): 0.255,
        (True, False, True): 0.44,
        (False, False, True): 0.036,
        (True, True, False): 0.303,
        (False, True, False): 0.262,
        (True, False, False): 0.044,
        (False, False, False): 0.48,
    }


def test_factor_product_mh():
    nodes_dict_temp = deepcopy(variable_elimination.nodes_dict)

    var_table_a = m_node.var_table
    table_a = m_node.table

    var_table_b = h_node.var_table
    table_b = h_node.table

    var_table, table = factor_product(
        nodes_dict_temp, var_table_a, table_a, var_table_b, table_b
    )

    # To make test easy we round values
    for key, value in table.items():
        table[key] = round(value, 3)

    assert var_table == "HM"

    # Variables: H - M
    assert table == {
        (True, True): 0.305,
        (True, False): 0.179,
        (False, True): 0.325,
        (False, False): 0.191,
    }


def test_factor_product_sd():
    nodes_dict_temp = deepcopy(variable_elimination.nodes_dict)

    var_table_a = s_node.var_table
    table_a = s_node.table

    var_table_b = d_node.var_table
    table_b = d_node.table

    var_table, table = factor_product(
        nodes_dict_temp, var_table_a, table_a, var_table_b, table_b
    )

    # To make test easy we round values
    for key, value in table.items():
        table[key] = round(value, 4)

    assert var_table == "DST"

    # Variables: D - S - T
    assert table == {
        (True, True, True): 0.0294,
        (True, True, False): 0.0035,
        (True, False, True): 0.1581,
        (True, False, False): 0.0279,
        (False, True, True): 0.0406,
        (False, True, False): 0.0665,
        (False, False, True): 0.7719,
        (False, False, False): 0.9021,
    }


def test_factor_product_ho_when_M_is_true():
    nodes_dict_temp = deepcopy(variable_elimination.nodes_dict)
    nodes_dict_temp["M"].is_true = True

    var_table_a, table_a = remove_known_values_to_get_f_table(
        nodes_dict_temp, h_node.var_table, h_node.table
    )

    var_table_b = o_node.var_table
    table_b = o_node.table

    var_table, table = factor_product(
        nodes_dict_temp, var_table_a, table_a, var_table_b, table_b
    )

    # To make test easy we round values
    for key, value in table.items():
        table[key] = round(value, 3)

    assert var_table == "HMO"

    # Variables: H - M - O
    assert table == {
        (True, True, True): 0.181,
        (False, True, True): 0.255,
        (True, True, False): 0.303,
        (False, True, False): 0.262,
    }
