import json


class Node:
    """The node class."""

    def __init__(self, identifier, name, var_table):
        self.identifier = identifier  # One letter describing the node
        self.name = name  # The node's description
        self.var_table = var_table  # The order of the variables in the table
        self.is_true = False  # Is the variable known as true
        self.is_false = False  # Is the variable known as false
        self.true_value = 50  # Probability of being true
        self.false_value = 50  # Probability of being false

    def initialized_table(self, table):
        self.table = table

    def __eq__(self, node):
        return self.identifier == node.identifier

    def __ne__(self, node):
        return self.identifier != node.identifier

    def __str__(self):
        return "({}) {}".format(self.identifier, self.name)

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(self.identifier)


# Creation of the nodes
# ------------------------------------------

m_node = Node("M", "Revenu mensuel inférieur à 2099 euros pas mois", "M")
h_node = Node("H", "L'individu est un homme", "H")
o_node = Node("O", "L'individu est obèse", "OMH")
a_node = Node("A", "Age supérieur à 65 ans", "A")
f_node = Node("F", "L'individu est fumeur", "F")
t_node = Node("T", "Hypertension artérielle", "TFOA")
d_node = Node("D", "Décés de l'individu", "DTS")
s_node = Node("S", "L'individu est suicidaire", "S")

# We initialize tables from the json file
# ------------------------------------------


def get_table(identifier, path):
    """Load json tables."""
    # We load tables
    with open(path) as f:
        data = json.load(f)

    table = dict()
    json_table_dict = data["nodes"][identifier]["table"]
    # An item is a line of the table

    for line in json_table_dict:
        tuple_boolean_values_list = list()

        # We need to seperate key and value
        for key, value in line.items():

            # We need to get boolean values
            tuple_str_values_string = key.split(",")
            for boolean_value_str in tuple_str_values_string:
                tuple_boolean_values_list.append(
                    True if boolean_value_str == "True" else False
                )

            # We create the tuple
            table[tuple(tuple_boolean_values_list)] = value

    return table
