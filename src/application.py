from tkinter import (BooleanVar, Checkbutton, Label, LabelFrame, filedialog,
                     messagebox)

from src.nodes import (a_node, d_node, f_node, get_table, h_node, m_node,
                       o_node, s_node, t_node)
from src.variable_elimination import Variable_elimination

# Config variables
# ------------------------------------------

# Colors used
BG_COLOR = "#212121"
NODE_TEXT_COLOR = "#ffffff"
IMPORTANT_COLOR = "#3e50b4"


# Variable elimination
# ------------------------------------------

# Creation of the variable_elimination object
variable_elimination = Variable_elimination()

# We add nodes to the variable_eliminiation object
variable_elimination.add_node(m_node)
variable_elimination.add_node(h_node)
variable_elimination.add_node(o_node)
variable_elimination.add_node(a_node)
variable_elimination.add_node(f_node)
variable_elimination.add_node(t_node)
variable_elimination.add_node(d_node)
variable_elimination.add_node(s_node)

# Creation of the controller (for true and false values / check buttons)
# ------------------------------------------


class Controller:
    def __init__(self):
        self.nodes_dict = dict()  # Node linked to its gui_node
        self.tables_are_loaded = False  # To know if table are loaded or not

    def add(self, node, gui_node):
        self.nodes_dict[node] = gui_node

    def update_gui(self):
        for node, node_gui in self.nodes_dict.items():
            node_gui.true_value.config(text="{} %".format(node.true_value))
            node_gui.false_value.config(text="{} %".format(node.false_value))


controller = Controller()

# Methods to create a graphical element for a node
# ------------------------------------------


class Node_GUI:
    def __init__(self, node, x_coord, y_coord, master_canvas, anchor):
        # The node
        self.node = node

        frame = LabelFrame(text=node.identifier,
                           borderwidth=2,
                           bg=BG_COLOR,
                           foreground=IMPORTANT_COLOR)

        # Title label
        title = Label(
            frame,
            text=node.name,
            font="OpenSans 13 bold",
            bg=BG_COLOR,
            foreground=NODE_TEXT_COLOR,
        )
        title.grid(row=1, column=1, sticky="W")

        # True and False labels
        true_label = Label(
            frame,
            text="True",
            font="OpenSans 11",
            bg=BG_COLOR,
            foreground=NODE_TEXT_COLOR,
        )
        false_label = Label(
            frame,
            text="False",
            fon="OpenSans 11",
            bg=BG_COLOR,
            foreground=NODE_TEXT_COLOR,
        )
        true_label.grid(row=2, column=1, sticky="W")
        false_label.grid(row=3, column=1, sticky="W")

        # True and False values
        self.true_value = Label(frame, text="##.##",
                                bg=BG_COLOR, foreground="#ff3f80")
        self.false_value = Label(
            frame, text="##.##", bg=BG_COLOR, foreground="#ff3f80")
        self.true_value.grid(row=2, column=1, sticky="N")
        self.false_value.grid(row=3, column=1, sticky="N")

        # True and False checkbuttons
        self.is_true = BooleanVar()
        self.is_false = BooleanVar()

        true_checkbutton = Checkbutton(frame,
                                       variable=self.is_true,
                                       bg=BG_COLOR,
                                       command=self.callback_true)
        false_checkbutton = Checkbutton(frame,
                                        variable=self.is_false,
                                        bg=BG_COLOR,
                                        command=self.callback_false)
        true_checkbutton.grid(row=2, column=1, sticky="E")
        false_checkbutton.grid(row=3, column=1, sticky="E")

        window = master_canvas.create_window(
            x_coord, y_coord, window=frame, anchor=anchor)
        self.x, self.y = master_canvas.coords(window)

    def callback_true(self):
        if controller.tables_are_loaded:
            self.node.is_true = self.is_true.get()
            # Because a node can not be true and false
            self.is_false.set(False)
            self.node.is_false = False

            variable_elimination.launch_algorithm()
            controller.update_gui()

        else:
            self.is_true.set(False)
            messagebox.showwarning(
                "Tables not loaded",
                "You need to load tables from a JSON file first. You can do"
                "it from the menu File, Load tables.",
            )

    def callback_false(self):
        if controller.tables_are_loaded:
            self.node.is_false = self.is_false.get()
            # Because a node can note be true and false
            self.is_true.set(False)
            self.node.is_true = False

            variable_elimination.launch_algorithm()
            controller.update_gui()

        else:
            self.is_false.set(False)
            messagebox.showwarning(
                "Tables not loaded",
                "You need to load tables from a JSON file first. You can do"
                "it from the menu File, Load tables.",
            )


# Menu creation functions
# ------------------------------------------


def name():
    messagebox.showinfo("Name", "Elie Kadoche")


def phone_number():
    messagebox.showinfo("Phone number", "+33 6 20 11 73 21")


def mail():
    messagebox.showinfo("Mail", "eliekadoche78@gmail.com")


def load_tables():
    path = filedialog.askopenfilename(
        title="Select JSON file for tables",
        filetypes=(("JSON files", "*.json"), ("All files", "*.*")),
    )

    try:
        m_node.initialized_table(get_table("M", path))
        h_node.initialized_table(get_table("H", path))
        o_node.initialized_table(get_table("O", path))
        a_node.initialized_table(get_table("A", path))
        f_node.initialized_table(get_table("F", path))
        t_node.initialized_table(get_table("T", path))
        d_node.initialized_table(get_table("D", path))
        s_node.initialized_table(get_table("S", path))

        variable_elimination.launch_algorithm()
        controller.update_gui()

        controller.tables_are_loaded = True

    except:
        messagebox.showerror(
            "Incorrect JSON file",
            "There is as error in the JSON file. Please correct it and try"
            "again.",
        )
