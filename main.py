from tkinter import LAST, Canvas, E, Menu, Tk, W

from src.application import (BG_COLOR, IMPORTANT_COLOR, Node_GUI, controller,
                             load_tables, mail, name, phone_number)
from src.nodes import (a_node, d_node, f_node, h_node, m_node, o_node, s_node,
                       t_node)

if __name__ == "__main__":
    # Creation of the main window
    # ------------------------------------------

    # Creation of the main window
    master_window = Tk()
    # master_window.resizable(False, False)
    master_window.title("Bayesian network")
    master_window.configure(background=BG_COLOR)

    # Creation of the menu
    # ------------------------------------------

    # Creation of the menu
    menu = Menu(master_window)
    master_window.config(menu=menu)

    # Creation of help menu
    help_menu = Menu(menu)
    menu.add_cascade(label="Contact", menu=help_menu)
    help_menu.add_command(label="Name", command=name)
    help_menu.add_command(label="Phone number", command=phone_number)
    help_menu.add_command(label="Mail", command=mail)

    # Creation of file menu
    file_menu = Menu(menu)
    menu.add_cascade(label="File", menu=file_menu)
    file_menu.add_command(label="Load tables", command=load_tables)

    # Creation of the main canvas
    # ------------------------------------------

    master_canvas = Canvas(
        master_window,
        width=750,
        height=542,
        bg=BG_COLOR,
        highlightthickness=0,
        relief="flat",
    )

    # Creation of the graphical nodes
    # ------------------------------------------

    # Creation of node M
    m_node_gui = Node_GUI(m_node, 350, 60, master_canvas, E)
    xm, ym = m_node_gui.x, m_node_gui.y
    controller.add(m_node, m_node_gui)

    # Creation of node H
    h_node_gui = Node_GUI(h_node, 350, 160, master_canvas, E)
    xh, yh = h_node_gui.x, h_node_gui.y
    controller.add(h_node, h_node_gui)

    # Creation of node O
    o_node_gui = Node_GUI(o_node, 570, 110, master_canvas, W)
    xo, yo = o_node_gui.x, o_node_gui.y
    controller.add(o_node, o_node_gui)

    # Creation of node A
    a_node_gui = Node_GUI(a_node, 720, 240, master_canvas, E)
    xa, ya = a_node_gui.x, a_node_gui.y
    controller.add(a_node, a_node_gui)

    # Creation of node F
    f_node_gui = Node_GUI(f_node, 715, 340, master_canvas, E)
    xf, yf = f_node_gui.x, f_node_gui.y
    controller.add(f_node, f_node_gui)

    # Creation of node T
    t_node_gui = Node_GUI(t_node, 350, 290, master_canvas, E)
    xt, yt = t_node_gui.x, t_node_gui.y
    controller.add(t_node, t_node_gui)

    # Creation of node D
    d_node_gui = Node_GUI(d_node, 575, 470, master_canvas, W)
    xd, yd = d_node_gui.x, d_node_gui.y
    controller.add(d_node, d_node_gui)

    # Creation of node S
    s_node_gui = Node_GUI(s_node, 350, 420, master_canvas, E)
    xs, ys = s_node_gui.x, s_node_gui.y
    controller.add(s_node, s_node_gui)

    # We draw arrows
    # ------------------------------------------

    line_M_O = master_canvas.create_line(
        xm, ym, xo, yo - 10, arrow=LAST, fill=IMPORTANT_COLOR
    )
    line_H_O = master_canvas.create_line(
        xh, yh, xo, yo, arrow=LAST, fill=IMPORTANT_COLOR)
    line_O_T = master_canvas.create_line(
        xo, yo + 10, xt, yt - 15, arrow=LAST, fill=IMPORTANT_COLOR
    )
    line_A_T = master_canvas.create_line(
        xa, ya - 30, xt, yt - 5, arrow=LAST, fill=IMPORTANT_COLOR
    )
    line_F_T = master_canvas.create_line(
        xf, yf + 30, xt, yt + 5, arrow=LAST, fill=IMPORTANT_COLOR
    )
    line_T_D = master_canvas.create_line(
        xt, yt + 15, xd, yd, arrow=LAST, fill=IMPORTANT_COLOR
    )
    line_S_D = master_canvas.create_line(
        xs, ys, xd, yd + 10, arrow=LAST, fill=IMPORTANT_COLOR
    )

    # We center the window, place the main canvas and run the application
    # ------------------------------------------

    w, h = 750, 542

    ws = master_window.winfo_screenwidth()
    hs = master_window.winfo_screenheight()

    x = (ws / 2) - (w / 2)
    y = (hs / 2) - (h / 2)

    master_window.geometry("%dx%d+%d+%d" % (w, h, x, y))

    master_canvas.pack()
    master_window.mainloop()
